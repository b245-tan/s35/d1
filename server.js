const express = require('express');

// Mongoose is a package that allows us to create schemas to model our data structures and to manipulate our database using different access methods
const mongoose = require('mongoose');

const port = 3001;
const app = express();


// [SECTION] MongoDB connection
	/*
		Syntax: 
			mongoose.connect("mongoDBconnectionString", {options to avoid errors in our connections})
	*/

	mongoose.connect("mongodb+srv://admin:admin@batch245-tan.bh8imqy.mongodb.net/s35-discussion?retryWrites=true&w=majority", {
			// Allows us to avoid any current and future errors while connecting to MONGODB
			useNewUrlParser: true,
			useUnifiedTopology: true
	})

	let db = mongoose.connection; 

	// error handling in connecting 
	db.on("error", console.error.bind(console, "Connection Error"));


	// this will be triggered if the connection is succesful.
	db.once("open", () => console.log("We're connected to the cloud database"))

		// Mongoose Schemas
			// Schema determine the structure of the documents to be written in the database
			// Schemas act s the blueprint to our data
		/*
			Syntax:
				const schemaName = new mongoose.Schema({keyvaluepairs})
		*/

		// taskSchema contains two properties: name & status
		// required is used to specify that a field must not be empty

		// default - is used if a field value is not supplied

		const taskSchema = new mongoose.Schema({
			name: {
				type: String,
				required :[true, "Task name is required!"]
			},
			status: {
				type: String,
				default: "pending"
			}
		})


// [SECTION] Models
	// Uses schema to create/instatiate documents/objects that follows our schema structure

	// the variable/object that will be created can be used to run commands with our database
	/*
		Syntax:
			const variableName = mongoose.model("collectionName", schemaName);
	*/

	const Task = mongoose.model("Task", taskSchema)

// middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true})) // it will allow our app to read data from forms.


// [SECTION] Routing
	// Create/add new task 
		/*
			1. Check if the task is existing.
				- if task already exists in the database, we will return a message "The task is already existing!"
				- if the task doesn't exist in the database, we will add it in the database.
		*/

	app.post("/tasks", (request, response) => {
		let input = request.body

		console.log(input.status);

		if(input.status === undefined) {
			Task.findOne({name: input.name}, (error, result) => {
				console.log(result);

				if(result !== null){
					return response.send("The task is already existing!")
				} else {
					let newTask = new Task({
						name: input.name
					})

					// save() method will save the object in the collection that the object instatiated
					newTask.save((saveError, savedTask) => {
						if(saveError){
							return console.log(saveError);
						}
						else {
							return response.send('New Task created!')
						}
					})
				}
			})
		}else {
			Task.findOne({name: input.name}, (error, result) => {
				console.log(result);

				if(result !== null){
					return response.send("The task is already existing!")
				} else {
					let newTask = new Task({
						name: input.name,
						status: input.status
					})

					// save() method will save the object in the collection that the object instatiated
					newTask.save((saveError, savedTask) => {
						if(saveError){
							return console.log(saveError);
						}
						else {
							return response.send('New Task created!')
						}
					})
				}
			})
		}
	})


// [SECTION] Retrieving all the tasks
	app.get("/tasks", (request, response) => {
		Task.find({}, (error, result) => {
			if(error) {
				console.log(error);
			}else{
				return response.send(result)
			}
		})
	})


// --------------------------------------------------------------------------------------------------------------------------------

// [ACTIVITY]

	// USER SCHEMA

	const userSchema = new mongoose.Schema({
			username: {
				type: String,
				required :[true, "Username is required!"]
			},
			password: {
				type: String,
				required :[true, "Password is required!"]
			}
		})


	// USER MODEL

	const User = mongoose.model("User", userSchema)


	app.post("/signup", (request, response) => {

		let signupInput = request.body;
		User.findOne({username: signupInput.username}, (error, result) => {
				console.log(result);

				if(result !== null){
					return response.send("Username is already taken!")
				} else {
					let newUser = new User({
						username: signupInput.username,
						password: signupInput.password
					})

					newUser.save((saveError, savedTask) => {
						if(saveError){
							return console.log(saveError);
						}
						else {
							return response.send('New user created!')
						}
					})
				}
			})
	})


app.listen(port, () => console.log(`Server is running at port ${port}.`))